using System;
namespace numbers
{
    public class MyMethods
    {
        public static void Calculator(){
            int num1;
            int num2;
            int num3;
            Console.WriteLine("---Mathmatical calculations Task-1--");
            Console.WriteLine("Enter the First value:");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Second value:");
            num2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Output:");
            num3= num1+num2;
            Console.WriteLine(num1+" + "+ num2 +"= "+num3);
            num3= num1-num2;
            Console.WriteLine(num1+" - "+ num2 +"= "+num3);
            num3= num1*num2;
            Console.WriteLine(num1+" * "+ num2 +"= "+num3);
            num3= num1/num2;
            Console.WriteLine(num1+" / "+ num2 +"= "+num3);
            num3= num1%num2;
            Console.WriteLine(num1+" % "+ num2 +"= "+num3);
            Console.WriteLine("------------------------------");
           
            Console.WriteLine("Do you want to Continue the Calculation Task-1 again(Y/N)?:");
            string YesOrNO = Console.ReadLine().ToUpper();
            string y = "Y";
            if(YesOrNO=="Y"){
            LoopingMethod(y,YesOrNO);
            }
            YesOrNO="";
        }
        public static void LoopingMethod(string y,string YesOrNO){
        while(YesOrNO == y){
           Calculator();
           YesOrNO="";
            }
        }

        public static void AverageOfTheNumber(){
            int d1,d2,d3,d4;
            int average;
            
            Console.WriteLine("--- Average value of the numbers Task-2--");
            Console.WriteLine("Enter the First number:");
            d1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Second number:");
            d2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the third number:");
            d3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the fourth number:");
            d4 = Convert.ToInt32(Console.ReadLine());
            average = (d1+d2+d3+d4)/4;
            Console.WriteLine("Output:");
            Console.WriteLine("The average value of this numbers {0}, {1}, {2}, {3} is = {4},",d1,d2,d3,d4,average);
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue the average value of the numbers Task-2 again(Y/N)?:");
            string YesOrNO1 = Console.ReadLine().ToUpper();
            string y1 = "Y";
            if(YesOrNO1=="Y"){
            LoopingMethod1(YesOrNO1,y1);
            }
            YesOrNO1="";
        }

        public static void LoopingMethod1(string YesOrNO1, string y1 ){
              while(YesOrNO1 == y1){
                  AverageOfTheNumber();
                  YesOrNO1="";
              }
        }

        public static void OddNumbers() { 
          Console.WriteLine("Print Odd numbers from 1 to 99. Task-3");
          Console.WriteLine("Output:");
          for (int n = 1; n < (99 + 1); n++)
            {
                if (n % 2 != 0)
                {
                    Console.WriteLine(n.ToString());
                }
            }
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue the odd numbers Task-3 again(Y/N)?:");
            string YesOrNOodd = Console.ReadLine().ToUpper();
            string yodd = "Y";
            if(YesOrNOodd=="Y"){
            LoopingMethodOdd(YesOrNOodd,yodd);
            }
            YesOrNOodd="";
            }
             public static void LoopingMethodOdd(string YesOrNOodd, string yodd ){
              while(YesOrNOodd == yodd){
                  OddNumbers();
                  YesOrNOodd="";
              }
        }

    public  static void CheckMultipleOfTheValue() {
           Console.WriteLine("Multiply of the Numbers Task-4");
           Console.WriteLine("\nInput first integer:");  
           int x = Convert.ToInt32(Console.ReadLine());
           Console.WriteLine("Output:");
           if (x > 0)
           {
              Console.WriteLine(x % 3 == 0 || x % 7 == 0);
           }
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue multiple of the number Task-4 again(Y/N)?:");
            string YesOrNOmul = Console.ReadLine().ToUpper();
            string ymul = "Y";
            if(YesOrNOmul=="Y"){
            LoopingMethodMul(YesOrNOmul,ymul);
            }
            YesOrNOmul="";
        }
     
        public static void LoopingMethodMul(string YesOrNOmul, string ymul ){
              while(YesOrNOmul == ymul){
                  CheckMultipleOfTheValue();
                  YesOrNOmul="";
              }
        }

        public static void LargestandLowestNum()
        {   
            Console.WriteLine("Largest and Lowest of the Numbers Task-5");
            Console.WriteLine("\nInput first integer:");  
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second integer:");  
            int y = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input third integer:");  
            int z = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine("Output:");
            Console.WriteLine("Largest of three: "+Math.Max(x, Math.Max(y, z)));
            Console.WriteLine("Lowest of three: "+Math.Min(x, Math.Min(y, z)));
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue Largest and Lowest of the Numbers Task-5 again(Y/N)?:");
            string YesOrNOLow = Console.ReadLine().ToUpper();
            string ylow = "Y";
            if(YesOrNOLow=="Y"){
            LoopingMethodLow(YesOrNOLow,ylow);
            }
            YesOrNOLow="";
        }

        public static void LoopingMethodLow(string YesOrNOLow, string ylow ){
              while(YesOrNOLow == ylow){
                  LargestandLowestNum();
                  YesOrNOLow="";
              }
        }

        public static void LargestOfTheNum()
        {
            int num1, num2, num3;
            Console.Write("Find the largest of three numbers Task-6");

            Console.Write("Input the 1st number :");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the 2nd number :");
            num2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the 3rd  number :");
            num3 = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Output:");
        if (num1 > num2)
            {
                if (num1 > num3)
                {
                    Console.Write("The 1st Number is the greatest among three. \n\n");
                }
                else
                {
                    Console.Write("The 3rd Number is the greatest among three. \n\n");
                }
            }
            else if (num2 > num3)
                Console.Write("The 2nd Number is the greatest among three \n\n");
            else
                Console.Write("The 3rd Number is the greatest among three \n\n");
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue Largest among the Three Numbers Task-6 again(Y/N)?:");
            string YesOrNOLarge = Console.ReadLine().ToUpper();
            string ylarge = "Y";
            if(YesOrNOLarge=="Y"){
            LoopingMethodLarge(YesOrNOLarge,ylarge);
            }
            YesOrNOLarge="";
        }
        public static void LoopingMethodLarge(string YesOrNOLarge, string ylarge ){
              while(YesOrNOLarge == ylarge){
                  LargestOfTheNum();
                  YesOrNOLarge="";
              }
        }
        public static void SubAndCalc(){
            Console.WriteLine("Subjects and Marks of the Students Task-7");
            Console.WriteLine("Enter the Roll Number of the Student:");
            int RollNO = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Name of the Student:");
            string Name = Console.ReadLine();
            Console.WriteLine("Enter the Marks of the Student in Physics:");
            int Physics = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Marks of the Student in Chemistry:");
            int Chemistry = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the Marks of the Student in Computer Application:");
            int Computer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Output:");
            Console.WriteLine("Roll No: {0},",RollNO);
            Console.WriteLine("Name of The Student: {0},",Name);
            Console.WriteLine("Scored Mark In Physics: {0},",Physics);
            Console.WriteLine("Scored Mark In Chemistry: {0},",Chemistry);
            Console.WriteLine("Scored Mark In Computer Application: {0},",Computer);
            int Total= Physics+Chemistry+Computer;
            Console.WriteLine("Total Marks = {0},",Total);
            int Percentage = Total/3;
            Console.WriteLine("Percentage = {0}.00,",Percentage);
            if(Percentage>60){
                 Console.WriteLine("Division = First,");
            }else{
                 Console.WriteLine("Division = Second,");
            }
            Console.WriteLine("------------------------------");
            Console.WriteLine("Do you want to continue Student Marks Task-7 again(Y/N)?:");
            string YesOrNOSub = Console.ReadLine().ToUpper();
            string ysub = "Y";
            if(YesOrNOSub=="Y"){
            LoopingMethodSub(YesOrNOSub,ysub);
            }
            YesOrNOSub="";
        }

        public static void LoopingMethodSub(string YesOrNOSub, string ysub ){
              while(YesOrNOSub == ysub){
                  SubAndCalc();
                  YesOrNOSub="";
              }
        }
    }

}